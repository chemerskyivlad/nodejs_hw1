const fs = require('fs');
const path = require('path');

const extensions = ['.log', '.txt', '.json', '.yaml', '.xml', '.js'];

function createFile(req, res, next) {
	if (!req.body || !req.body.filename || !req.body.content || !extensions.includes(path.extname(req.body.filename))) {
		return res.status(400).send({
			message: "Please specify 'content' parameter"
		});
	}
	fs.writeFile(`./files/${req.body.filename}`, req.body.content, (err) => {
		if (err) {
			return res.status(500).send({
				message: "Server error"
			});
		}
	});
	res.status(200).send({
		"message": "File created successfully"
	});
}

function getFiles(req, res, next) {
	fs.readdir(`./files`, (err, files) => {
		if (err) {
			return res.status(500).send({
				message: "Server error"
			});
		}
		if (!files) {
			return res.status(400).send({
				message: "Client error"
			});
		}
		res.status(200).send({
			"message": "Success",
			"files": files
		});
	});
}

const getFile = (req, res, next) => {
	let link = `./files/${req.params.filename}`;

	if (!fs.existsSync(link)) {
		return res.status(400).send({
			message: `No file with '${req.params.filename}' filename found`
		});
	}

	let data = fs.readFileSync(link, 'utf8');
	let extension = path.extname(link).slice(1);
	const {
		birthtime
	} = fs.statSync(link);

	res.status(200).send({
		"message": "Success",
		"filename": req.params.filename,
		"content": data,
		"extension": extension,
		"uploadedDate": birthtime
	});
}

const editFile = (req, res, next) => {
	let link = `./files/${req.params.filename}`;

	if (!fs.existsSync(link)) {
		return res.status(400).send({
			message: `No file with '${req.params.filename}' filename found`
		});
	}
	if (!req.body || !req.body.content) {
		return res.status(400).send({
			message: "Please specify 'content' parameter"
		});
	}
	fs.writeFileSync(link, req.body.content);
	res.status(200).send({
		message: "File successfully updated"
	});
}

const deleteFile = (req, res, next) => {
	fs.unlink(`./files/${req.params.filename}`, (err) => {
		if (err) {
			return res.status(500).send({
				message: "Server error"
			});
		}

		res.status(200).send({
			message: "File successfully deleted"
		});
	});
}

module.exports = {
	createFile,
	getFiles,
	getFile,
	editFile,
	deleteFile
}